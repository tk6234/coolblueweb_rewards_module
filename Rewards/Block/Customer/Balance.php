<?php

namespace CoolBlueWeb\Rewards\Block\Customer;

use \Magento\Framework\View\Element\Template;

class Balance extends Template
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \CoolBlueWeb\Rewards\Model\BalanceFactory
     */
    protected $_rewardsBalanceFactory;

    /**
     * @param \Template\Context $context
     * @param \CoolBlueWeb\Rewards\Helper\Customer\RewardsBalance $rewardsBalance
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \CoolBlueWeb\Rewards\Model\Customer\BalanceFactory $rewardsBalanceFactory,
        \Magento\Customer\Model\Session $customerSession
     ) {
        parent::__construct($context);
        $this->_rewardsBalanceFactory  = $rewardsBalanceFactory;
        $this->_customerSession        = $customerSession;
    }

    public function rewardBalance()
    {
        $customerId = $this->_customerSession->getCustomerId();

        if($customerId) {
            $balance = $this->_rewardsBalanceFactory->create();
            $balance->getBalanceByCustomerId($customerId);
            return $balance->getFormattedBalance();
        }
    }

    public function customerExistsInSession()
    {
        return (boolean) $this->_customerSession->getCustomerId();
    }
}
