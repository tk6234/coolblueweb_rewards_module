<?php

namespace CoolBlueWeb\Rewards\Block\Promo;

use \Magento\Framework\View\Element\Template;

class Product extends Template
{
    /**
     * @var \CoolBlueWeb\Rewards\Rewards
     */
    protected $_rewardsHelper;

    /**
     * @var\Magento\Framework\Registry
     */
     protected $_coreRegistry;

    /**
     * @param \Template\Context $context
     * @param \CoolBlueWeb\Rewards\Helper\Customer\RewardsBalance $rewardsBalance
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \CoolBlueWeb\Rewards\Helper\RewardsHelper $rewards,
        \Magento\Framework\Registry $registry
     ) {
        parent::__construct($context);
        $this->_rewardsHelper   = $rewards;
        $this->_coreRegistry    = $registry;
    }

    /**
     * Retrieve product
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    public function getPoints()
    {
        $product = $this->getProduct();
        $rewardPoints  = $this->_rewardsHelper->getRewardsValueByAmount(
            $product->getPriceModel()->getFinalPrice(1, $product)
        );

        return $this->_rewardsHelper->getRewardCurrency($rewardPoints);
    }
}
