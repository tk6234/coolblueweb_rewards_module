<?php

namespace CoolBlueWeb\Rewards\Api\Data;

interface CustomerBalanceInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const ATTRIBUTE_CODE = 'coolblue_rewards_balance';

    /**
     * Retrieve the current rewards balance value.
     *
     * @return float
     */
    public function getBalance();

    /**
     * Retrieve the current customer.
     *
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer();

    /**
     * Retrieve the current rewards balance with the correct currency format.
     *
     * @return string
     */
    public function getFormattedBalance();

    /**
     * Retrieve the rewards balance for a customer by customer id.
     *
     * @param int|float $amount
     */
    public function getBalanceByCustomerId($amount);

    /**
     * Subtract amount from a customer's rewards balance.
     *
     * @param int|float $amount
     */
    public function subtractFromBalance($amount);

    /**
     * Add amount to a customer's rewards balance.
     *
     * @param int|float $amount
     */
    public function addToBalance($amount);

    /**
     * Subtract amount of reward percentage from a customer's rewards balance.
     *
     * @param int|float $amount
     */
    public function subtractPercentage($amount);

    /**
     * Add amount of reward percentage to a customer's rewards balance.
     *
     * @param int|float $amount
     */
    public function addPercentage($amount);

    /**
     * Save a customer's balance
     */
    public function save();
}
