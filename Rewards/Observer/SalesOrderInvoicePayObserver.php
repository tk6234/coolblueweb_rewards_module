<?php

namespace CoolBlueWeb\Rewards\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \CoolBlueWeb\Rewards\Model\Customer\Balance;
use \Magento\Framework\Event\Observer;

class SalesOrderInvoicePayObserver implements ObserverInterface
{
    /**
     * @var \CoolBlueWeb\Rewards\Helper\Customer\RewardsBalance
     */
    protected $customerRewardsBalance;

    /**
     * @param \CoolBlueWeb\Rewards\Helper\Customer\RewardsBalance
     */
    public function __construct(Balance $customerRewardsBalance)
    {
        $this->customerRewardsBalance = $customerRewardsBalance;
    }

    /**
     * @param \Magento\Framework\Event\Observer
     */
    public function execute(Observer $observer)
    {
        $invoice  = $observer->getEvent()->getInvoice();

        if($customerId = $invoice->getOrder()->getCustomerId())
        {
            $this->customerRewardsBalance->getBalanceByCustomerId($customerId);

            // subtract reward points used for order from current balance
            if($invoice->getOrder()->getCoolblueRewardsUsed()) {
                $this->customerRewardsBalance->subtractFromBalance(
                    $invoice->getOrder()->getCoolblueRewardsUsed()
                );
            }

            // add reward points to balance for new order 
            $this->customerRewardsBalance->addPercentage(
                $invoice->getOrder()->getTotalPaid()
            );

            $this->customerRewardsBalance->save();
        }
    }
}
