<?php

namespace CoolBlueWeb\Rewards\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;
use \CoolBlueWeb\Rewards\Helper\OrderHelper;

class SalesEventQuoteSubmitBeforeObserver implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer
     */
    public function execute(Observer $observer)
    {
        $order  = $observer->getEvent()->getOrder();
        $quote  = $observer->getEvent()->getQuote();
        $order->setData(OrderHelper::ATTRIBUTE_CODE, $quote->getData(OrderHelper::ATTRIBUTE_CODE));
    }
}
