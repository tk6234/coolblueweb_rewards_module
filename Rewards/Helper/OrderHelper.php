<?php
namespace CoolBlueWeb\Rewards\Helper;

use \Magento\Customer\Api\CustomerRepositoryInterface;

class OrderHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    const ATTRIBUTE_CODE    = 'coolblue_rewards_used';
}
