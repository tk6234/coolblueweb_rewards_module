<?php
namespace CoolBlueWeb\Rewards\Helper;

use \Magento\Customer\Api\CustomerRepositoryInterface;

class RewardsHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_pricingHelper;

    /**
     * @param \Magento\Framework\Pricing\Helper\Data
     */
    public function __construct(\Magento\Framework\Pricing\Helper\Data $pricingHelper)
    {
        $this->_pricingHelper = $pricingHelper;
    }

    /**
     * @param float
     */
    public function getRewardsValueByAmount($amount)
    {
        return $amount * ($this->getRewardRate() / 100);
    }

    /**
     * @return int
     */
    public function getRewardRate()
    {
        // TODO - get value from core_data_config
        return 5;
    }

    public function getRewardCurrency($rewardPoints)
    {
        return $this->_pricingHelper->currency($rewardPoints,true,false);
    }
}
