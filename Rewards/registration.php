<?php
/**
 * Registration overview
 *
 * Magento components, including modules, themes, and language packages, must be registered in the Magento system through the Magento ComponentRegistrar class.
 *
 * Each component must have a file called registration.php in its root directory.
 *
 * @see http://devdocs.magento.com/guides/v2.1/extension-dev-guide/build/component-registration.html
 *
 *
 * Component Types:
 *
 * const MODULE   = 'module';
 * const LIBRARY  = 'library';
 * const THEME    = 'theme';
 * const LANGUAGE = 'language';
 */

 use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'CoolBlueWeb_Rewards',
    __DIR__
);
