<?php

namespace CoolBlueWeb\Rewards\Model\Customer;

class Balance implements \CoolBlueWeb\Rewards\Api\Data\CustomerBalanceInterface
{
    /**
     * @var float
     */
    protected $_balance;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customer;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var \CoolBlueWeb\Rewards\Helper\RewardsHelper
     */
    protected $_rewardsHelper;

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \CoolBlueWeb\Rewards\Helper\RewardsHelper $rewardsHelper
    ) {

        $this->_customerRepository = $customerRepository;
        $this->_rewardsHelper      = $rewardsHelper;
    }

    /**
     * Retrieve the current rewards balance value.
     *
     * @return float
     */
    public function getBalance()
    {
        return $this->_balance;
    }

    /**
     * Retrieve the current customer.
     *
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        return $this->_customer;
    }

    /**
     * Retrieve the current rewards balance with the correct currency format.
     *
     * @return string
     */
    public function getFormattedBalance()
    {
        return $this->_rewardsHelper->getRewardCurrency($this->getBalance());
    }

    /**
     * Retrieve the rewards balance for a customer by customer id.
     *
     * @param int $customerId
     */
    public function getBalanceByCustomerId($customerId)
    {
        $this->_customer = $this->_customerRepository->getById($customerId);
        $this->_balance  = ($balance = $this->_customer->getCustomAttribute(self::ATTRIBUTE_CODE)) ? $balance->getValue() : 0;
        return $this->_balance;
    }

    /**
     * Subtract amount from a customer's rewards balance.
     *
     * @param int|float $amount
     */
    public function subtractFromBalance($amount)
    {
        $this->_balance = ($this->_balance - $amount);
        return $this;
    }

    /**
     * Add amount to a customer's rewards balance.
     *
     * @param int|float $amount
     */
    public function addToBalance($amount)
    {
        $this->_balance += $amount;
        return $this;
    }

    /**
     * Subtract amount of reward percentage from a customer's rewards balance.
     *
     * @param int|float $amount
     */
    public function subtractPercentage($amount)
    {
        $this->subtractFromBalance(
            $this->_rewardsHelper->getRewardsValueByAmount($amount)
        );
    }

    /**
     * Add amount of reward percentage to a customer's rewards balance.
     *
     * @param int|float $amount
     */
    public function addPercentage($amount)
    {
        $this->addToBalance(
            $this->_rewardsHelper->getRewardsValueByAmount($amount)
        );
    }

    /**
     * Save a customer's balance
     */
    public function save()
    {
        $this->getCustomer()->setCustomAttribute(self::ATTRIBUTE_CODE, $this->getBalance());
        $this->_customerRepository->save($this->getCustomer());
    }
}
