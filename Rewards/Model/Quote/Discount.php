<?php
namespace CoolBlueWeb\Rewards\Model\Quote;

class Discount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    const DISCOUNT_CODE  = 'coolbluerewards';

    const DISCOUNT_LABEL = 'Coolblue Rewards Discount';

    /**
     * @var \CoolBlueWeb\Rewards\Helper\Customer\RewardsBalance
     */
    protected $rewardBalance;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\SalesRule\Model\Validator
     */
    protected $validator;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\SalesRule\Model\Validator $validator,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \CoolBlueWeb\Rewards\Model\Customer\Balance $rewardBalance
    ) {
        $this->setCode(self::DISCOUNT_CODE);
        $this->eventManager     = $eventManager;
        $this->calculator       = $validator;
        $this->storeManager     = $storeManager;
        $this->priceCurrency    = $priceCurrency;
        $this->rewardBalance    = $rewardBalance;
    }

    public function collect(
     \Magento\Quote\Model\Quote $quote,
     \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
     \Magento\Quote\Model\Quote\Address\Total $total
     ) {
         parent::collect($quote, $shippingAssignment, $total);
         $address       = $shippingAssignment->getShipping()->getAddress();
         $label         = self::DISCOUNT_LABEL;


         $TotalAmount   = $this->rewardBalance->getBalanceByCustomerId($quote->getCustomer()->getId());

         $discountAmount ="-".$TotalAmount;
         $appliedCartDiscount = 0;

         if($total->getDiscountDescription()) {
            $appliedCartDiscount = $total->getDiscountAmount();
            $discountAmount = $total->getDiscountAmount()+$discountAmount;
            $label = $total->getDiscountDescription().', '.$label;
          }

             $total->setDiscountDescription($label);
             $total->setDiscountAmount($discountAmount);
             $total->setBaseDiscountAmount($discountAmount);
             $total->setSubtotalWithDiscount($total->getSubtotal() + $discountAmount);
             $total->setBaseSubtotalWithDiscount($total->getBaseSubtotal() + $discountAmount);
             $quote->setData(\CoolBlueWeb\Rewards\Helper\OrderHelper::ATTRIBUTE_CODE, $TotalAmount);

         if(isset($appliedCartDiscount)) {
             $total->addTotalAmount($this->getCode(), $discountAmount - $appliedCartDiscount);
             $total->addBaseTotalAmount($this->getCode(), $discountAmount - $appliedCartDiscount);
         }
         else {
            $total->addTotalAmount($this->getCode(), $discountAmount);
            $total->addBaseTotalAmount($this->getCode(), $discountAmount);
         }
         return $this;
     }

     public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
     {
         $result = null;
         $amount = $total->getDiscountAmount();

         if ($amount != 0) {
             $description = $total->getDiscountDescription();
             $result = [
                 'code'  => $this->getCode(),
                 'title' => strlen($description) ? __(self::DISCOUNT_LABEL . ' (%1)', $description) : __(self::DISCOUNT_LABEL),
                 'value' => $amount
             ];
         }
         return $result;
     }
}
