<?php
namespace CoolBlueWeb\Rewards\Setup;

use \CoolBlueWeb\Rewards\Model\Customer\Balance;
use \CoolBlueWeb\Rewards\Helper\OrderHelper;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
    /**
     * Attribute setup factory
     *
     * @var \Magento\Eav\Model\Entity\Attribute\SetFactory
     */
    protected $attributeSetFactory;

    /**
     * Customer setup factory
     *
     * @var \Magento\Customer\Setup\CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var \Magento\Sales\Setup\SalesSetupFactory
     */
    protected $salesSetupFactory;

    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory,
        \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory
    ) {

        $this->attributeSetFactory   = $attributeSetFactory;
        $this->customerSetupFactory  = $customerSetupFactory;
        $this->salesSetupFactory     = $salesSetupFactory;
    }

    public function install(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {

        $this->createOrderAttributes($setup, $context);
        $this->createCustomerAttributes($setup, $context);
        $this->createRewardsQuoteColumn($setup, $context);
    }

    protected function createRewardsQuoteColumn($setup, $context)
    {
        $setup->getConnection()
           ->addColumn(
               $setup->getTable('quote'),
               OrderHelper::ATTRIBUTE_CODE,
               [
                   'type'    => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                   'length'  => '12,4',
                   'comment' =>'Coolblue rewards used'
               ]
           );
         $setup->endSetup();
    }

    protected function createCustomerAttributes($setup, $context)
    {
        $attributeSet     = $this->attributeSetFactory->create();
        $customerSetup    = $this->customerSetupFactory->create(['setup' => $setup]);
        $customerEntity   = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId   = $customerEntity->getDefaultAttributeSetId();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

       /**
        * @param string|integer $entityTypeId
        * @param string $code
        * @param array $attr
        * @return $this
        */
        $customerSetup->addAttribute(
			\Magento\Customer\Model\Customer::ENTITY,
			Balance::ATTRIBUTE_CODE,
			[
				'type'          => 'decimal',
				'label'         => 'CoolBlue Rewards Balance',
				'input'         => 'text',
				'source'        => '',
                'backend'       => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
				'required'      => false,
				'default'       => '0',
				'sort_order'    => 100,
				'system'        => false,
				'position'      => 100,
                'user_defined'  => false,
			]
		);

        $attribute = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, Balance::ATTRIBUTE_CODE)
           ->addData([
              'attribute_set_id'   => $attributeSetId,
              'attribute_group_id' => $attributeGroupId,
              'used_in_forms'      => ['adminhtml_customer'],
           ]);

        $attribute->save();
    }

    protected function createOrderAttributes($setup, $context)
    {
        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);

        $salesSetup->addAttribute(\Magento\Sales\Model\Order::ENTITY, OrderHelper::ATTRIBUTE_CODE, [
        'type'      => 'decimal',
        'required'  => false,
        'visible'   => true,
     ]);
    }
}
